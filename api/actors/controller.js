const logger = require('../../config/logger')
const actors = require('./model')

var getAllActors = async () => {
  return actors.find({}, err => err ? logger.err(err) : '_')
}

var getActor = async (id) => {
  return actors.findById(parseInt(id))
}

var updateActor = async (actor) => {
  const keys = Object.keys(actor)
  let result = 400
  if (keys.length === 2 && keys.includes('id') && keys.includes('avatar_url')) {
    result = (await actors.findByIdAndUpdate(actor.id, actor) ? 200 : 404)
  }
  return result
}

var getStreak = () => {
  return actors.find({}, err => err ? logger.err(err) : '_')
}

module.exports = {
  updateActor: updateActor,
  getAllActors: getAllActors,
  getStreak: getStreak,
  getActor: getActor
}
