const mongoose = require('mongoose')

const { Schema } = mongoose

const actorSchema = new Schema({
  _id: { type: Number, required: true },
  login: { type: String },
  avatar_url: { type: String }
})

actorSchema.virtual('id').get(function () {
  return this._id
})

actorSchema.virtual('id').set(function (v) {
  this._id = v
})

actorSchema.options.toJSON = {
  virtuals: true,
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  }
}

module.exports = mongoose.model('Actor', actorSchema)
