var express = require('express')
var router = express.Router()

const controller = require('./controller')

// Routes related to actor.
router.get('/', async function (req, res, next) {
  try {
    res.json(await controller.getAllActors())
  } catch (error) {
    next(error)
  }
})

router.put('/', async function (req, res, next) {
  try {
    res.status(await controller.updateActor(req.body)).send()
  } catch (error) {
    next(error)
  }
})

router.get('/streak', async function (req, res, next) {
  try {
    res.json(await controller.getAllActors())
  } catch (error) {
    next(error)
  }
})

module.exports = router
