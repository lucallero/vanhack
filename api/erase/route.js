var express = require('express')
var router = express.Router()

const controller = require('./controller')

// Route related to delete events
router.delete('/', async function (req, res, next) {
  try {
    res.status(await controller.eraseEvents()).send()
  } catch (error) {
    next(error)
  }
})

module.exports = router
