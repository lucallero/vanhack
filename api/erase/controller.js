const service = require('../events/service')
const logger = require('../../config/logger')

const eraseEvents = async (req, res) => {
  const info = await service.eraseEvents()
  logger.debug(JSON.stringify(info))
  return 200
}
module.exports = { eraseEvents }
