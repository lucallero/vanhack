/* eslint-disable no-sequences */
const logger = require('../../config/logger')
const events = require('./model')
const actors = require('../actors/model')
const DUPLICATE_KEY_ERROR = 11000

var getAllEvents = async () => {
  return events.find({}).populate('actor').sort({ _id: 1 })
}

var addEvent = async (body) => {
  return saveEvent(splitEvent(body))
}

var getByActor = (actorId) => {
  return events.find({ actor: actorId }).populate('actor').sort({ _id: 1 })
}

var eraseEvents = async () => {
  return events.deleteMany({}, err => err ? logger.error(err) : logger.info('Events cleared.'))
}

const splitEvent = (body) => {
  const { actor, actorId = actor.id, ...event } = body
  event.actor = actorId
  return { event: event, actor: actor }
}

const saveEvent = async (splitedEvent) => {
  try {
    await actors.create(splitedEvent.actor)
  } catch (err) {
    if (!err.code && err.code === DUPLICATE_KEY_ERROR) {
      throw err
    }
  }
  return events.create(splitedEvent.event)
}

module.exports = {
  getAllEvents,
  addEvent,
  saveEvent,
  getByActor,
  eraseEvents,
  splitEvent
}
