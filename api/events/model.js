const mongoose = require('mongoose')
const { Schema } = mongoose

const repoSchema = new Schema({
  _id: { type: Number },
  name: { type: String },
  url: { type: String }
})

repoSchema.virtual('id').get(function () {
  return this._id
})

repoSchema.virtual('id').set(function (v) {
  this._id = v
})

repoSchema.options.toJSON = {
  virtuals: true,
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  }
}

const eventSchema = new Schema({
  _id: { type: Number },
  type: { type: String },
  actor: { type: Number, ref: 'Actor', required: true, index: true },
  repo: repoSchema,
  created_at: { type: String }
})

eventSchema.virtual('id').get(function () {
  return this._id
})

eventSchema.virtual('id').set(function (v) {
  this._id = v
})

eventSchema.options.toJSON = {
  virtuals: true,
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  }
}

module.exports = mongoose.model('Event', eventSchema)
