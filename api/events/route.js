var express = require('express')
var router = express.Router()

const controller = require('./controller')

// Routes related to event
router.get('/', async function (req, res, next) {
  try {
    res.json(await controller.getAllEvents(req, res))
  } catch (error) {
    next(error)
  }
})

router.post('/', async function (req, res, next) {
  try {
    res.status(await controller.addEvent(req, res)).send()
  } catch (error) {
    next(error)
  }
})

router.get('/actors/:id', async function (req, res, next) {
  try {
    const result = await controller.getByActor(req, res)
    result === 404 ? res.status(result).send() : res.json(result)
  } catch (error) {
    next(error)
  }
})

module.exports = router
