const service = require('./service')
const logger = require('../../config/logger')
const DUPLICATE_KEY_ERROR = 11000
const actor = require('../actors/controller')

const addEvent = async (req, res) => {
  let result = 201
  try {
    await service.addEvent(req.body)
  } catch (err) {
    logger.debug(JSON.stringify(err))
    result = (err.code && err.code === DUPLICATE_KEY_ERROR) ? 400 : 500
  }
  return result
}

const getAllEvents = async (req, res) => {
  return service.getAllEvents()
}

const getByActor = async (req, res) => {
  return await actor.getActor(req.params.id)
    ? service.getByActor(req.params.id)
    : 404
}

module.exports = { addEvent, getAllEvents, getByActor }
