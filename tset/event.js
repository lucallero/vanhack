/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-unused-expressions */
const chai = require('chai')
const expect = require('chai').expect
const should = require('chai').should()
const service = require('../api/events/service')
const model = require('../api/events/model')

describe('Should test Events.', () => {
  it('Should split body in two objects, actor and event.', async () => {
    const result = service.splitEvent(body)
    expect(result).to.have.property('event')
    expect(result).to.have.property('actor')
    result.actor.id.should.equal(body.actor.id)
    result.event.id.should.equal(body.id)
  })

  it('Should create two objects in different collections, Actors and Event.', async () => {
    const splitedEvent = service.splitEvent(body)
    let result = await service.saveEvent(splitedEvent)
    result = await model.findByIdAndDelete(body.id)
    result = await model.findById(body.id)
    expect(result).to.be.null
  })

  it('Should create given Event in the database.', async () => {
    let result = await model.create(service.splitEvent(body).event)
    expect(result.id).to.equal(body.id)
    result = await model.findByIdAndDelete(body.id)
    result = await model.findById(body.id)
    expect(result).to.be.null
  })

  it('Should test getAllEvents, create 10 events and query all 10 soon after.', async () => {
    for (let i = 0; i < 10; i++) {
      body.id = i
      const result = await model.create(service.splitEvent(body).event)
    }
    const result = await service.getAllEvents()
    expect(result.length).to.equal(10)
    model.deleteMany({}, err => console.log(err))
  })

  it('should failed to insert an Event that already exist', async () => {
    const DUPLICATE_KEY_ERROR = 11000
    let result
    try {
      result = await model.create(service.splitEvent(body).event)
      result.id.should.equal(body.id)
      await model.create(service.splitEvent(body).event)
    } catch (error) {
      console.log(error.message)
      error.code.should.equal(DUPLICATE_KEY_ERROR)
    } finally {
      await model.findByIdAndDelete(body.id)
      result = await model.findById(body.id)
      expect(result).to.be.null
    }
  })
})

const body = {
  id: 4055191679,
  type: 'PushEvent',
  actor: {
    id: 2790311,
    login: 'daniel33',
    avatar_url: 'https://avatars.com/2790311'
  },
  repo: {
    id: 352806,
    name: 'johnbolton/exercitationem',
    url: 'https://github.com/johnbolton/exercitationem'
  },
  created_at: '2015-10-03 06:13:31'
}
