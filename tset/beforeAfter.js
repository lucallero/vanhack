/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const mongoose = require('mongoose')
const { MongoMemoryServer } = require('mongodb-memory-server')
before((done) => {
  mongoose.set('useNewUrlParser', true)
  mongoose.set('useFindAndModify', false)
  mongoose.set('useCreateIndex', true)
  mongoose.set('useUnifiedTopology', true)
  // to test with a local mongoDB
  // mongoose.connect('mongodb://localhost:27017/vanhack', (err) => {
  //   if (err) console.log('Database connection failed:', err)
  // }).then(() => done())
  mongoServer = new MongoMemoryServer({
    binary: { version: '4.0.3' }
  })
  mongoServer.getConnectionString().then((mongoUri) => {
    return mongoose.connect(mongoUri, null, (err) => {
      if (err) done(err)
    })
  }).then(() => done())
})
after(() => {
  mongoose.disconnect()
  mongoServer.stop()
})
