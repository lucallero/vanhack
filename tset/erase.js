/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-unused-expressions */
const chai = require('chai')
const expect = require('chai').expect
const should = require('chai').should()
const model = require('../api/events/model')
const controller = require('../api/erase/controller')
const eventService = require('../api/events/service')

describe('Should test Erase controller.', () => {
  it('Collection should be empty after calling controller.erase().', async () => {
    for (let i = 0; i < 10; i++) {
      body.id = i
      const result = await model.create(eventService.splitEvent(body).event)
    }
    let result = await model.find()
    expect(result.length).to.equal(10)
    controller.eraseEvents()
    result = await model.find()
    expect(result.length).to.equal(0)
  })
})

const body = {
  id: 4055191679,
  type: 'PushEvent',
  actor: {
    id: 2790311,
    login: 'daniel33',
    avatar_url: 'https://avatars.com/2790311'
  },
  repo: {
    id: 352806,
    name: 'johnbolton/exercitationem',
    url: 'https://github.com/johnbolton/exercitationem'
  },
  created_at: '2015-10-03 06:13:31'
}
