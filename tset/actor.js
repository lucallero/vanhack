/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-unused-expressions */
const chai = require('chai')
const expect = require('chai').expect
const should = require('chai').should()
const model = require('../api/actors/model')
const controller = require('../api/actors/controller')

describe('Should test Actors.', () => {
  it('Should check if mongoose model and db are properly CRUDing.', async () => {
    const newActor = await model.create({ id: 123, login: 'Paulo', avatar_url: 'https://myface.com' })
    let result = await model.findByIdAndUpdate(123, { avatar_url: 'https://anotherface' })
    expect(result.id).to.equal(newActor.id)
    result = await model.findByIdAndDelete(123)
    expect(result.avatar_url).to.equal('https://anotherface')
    expect(result.id).to.equal(123)
    result = await model.findById(123)
    expect(result).to.be.null
  })

  it('Should try to update an invalid field of an Actor and receive 400.', async () => {
    const result = await controller.updateActor({ id: 123, login: 'Paulo', avatar_url: 'https://myface.com' })
    result.should.equal(400)
  })
  it('Should try to update an invalid field of an Actor an receive 400.', async () => {
    const result = await controller.updateActor({ id: 123, login: 'Paulo' })
    result.should.equal(400)
  })

  it('Should update an Actor an receive 200.', async () => {
    const newActor = await model.create({ id: 123, login: 'Paulo', avatar_url: 'https://myface.com' })
    const result = await controller.updateActor({ id: 123, avatar_url: 'https://otherface.com' })
    result.should.equal(200)
  })

  it('Should try to an inexistent Actor an receive 404.', async () => {
    const result = await controller.updateActor({ id: 124, avatar_url: 'https://myface.com' })
    result.should.equal(404)
  })
})
