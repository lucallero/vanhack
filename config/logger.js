const { createLogger, format, transports } = require('winston')

const LOG_LEVEL = process.env.LOG_LEVEL || 'debug'

const options = {
  console: {
    level: LOG_LEVEL,
    handleExceptions: true,
    json: false,
    format: format.combine(
      format.colorize(),
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    )
  }
}

const logger = createLogger({
  transports: [
    new transports.Console(options.console)
  ],
  exitOnError: false
})

module.exports = logger
