/* eslint-disable import/no-extraneous-dependencies */
const mongoose = require('mongoose')
const logger = require('./logger')
const NODE_ENV = (process.env.NODE_ENV ? process.env.NODE_ENV : 'local')

mongoose.Promise = global.Promise

mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose.set('useUnifiedTopology', true)

if (NODE_ENV === 'local') {
  // eslint-disable-next-line global-require
  const { MongoMemoryServer } = require('mongodb-memory-server')
  const mongoServer = new MongoMemoryServer()
  mongoServer.getConnectionString().then((mongoUri) => {
    mongoose.connect(mongoUri)
  })
} else {
  mongoose.connect(process.env.DB_URL)
}

const db = mongoose.connection

// Bind connection to error event (to get notification of connection errors)
// eslint-disable-next-line no-console
db.on('error', console.error.bind(console, 'MongoDB connection error:'))
logger.info('DB configuration complete.')
